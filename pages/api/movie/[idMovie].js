/**
* @swagger
* /api/movie/{idMovie}:
*   get:
*     summary: Retrieve a specific movie by ID
*     description: Get detailed information about a specific movie.
*     parameters:
*       - in: path
*         name: idMovie
*         required: true
*         schema:
*           type: string
*         description: The movie's ID.
*     responses:
*       200:
*         description: Detailed information about the movie.
*         content:
*           application/json:
*             schema:
*               $ref: '#/components/schemas/Movie'
*       404:
*         description: The movie was not found.
*       500:
*         description: An error occurred on the server.
*
*   post:
*     summary: Add a new movie
*     description: Create a new movie entry in the database.
*     requestBody:
*       required: true
*       content:
*         application/json:
*           schema:
*             $ref: '#/components/schemas/Movie'
*     responses:
*       201:
*         description: The movie was successfully created.
*       400:
*         description: Missing required fields.
*       500:
*         description: An error occurred on the server.
*
*   put:
*     summary: Update a movie
*     description: Update details of an existing movie.
*     parameters:
*       - in: path
*         name: idMovie
*         required: true
*         schema:
*           type: string
*         description: The movie's ID.
*     requestBody:
*       required: true
*       content:
*         application/json:
*           schema:
*             $ref: '#/components/schemas/Movie'
*     responses:
*       200:
*         description: The movie was successfully updated.
*       404:
*         description: The movie was not found.
*       500:
*         description: An error occurred on the server.
*
*   delete:
*     summary: Delete a movie
*     description: Remove a movie entry from the database.
*     parameters:
*       - in: path
*         name: idMovie
*         required: true
*         schema:
*           type: string
*         description: The movie's ID.
*     responses:
*       204:
*         description: The movie was successfully deleted.
*       404:
*         description: The movie was not found.
*       500:
*         description: An error occurred on the server.
*/

import { ObjectId } from 'mongodb';
import clientPromise from '../../../lib/mongodb';

// Handler pour les opérations CRUD sur un film spécifique
export default async function handler(req, res) {
    const { query: { idMovie }, method, body } = req;

    const client = await clientPromise;
    const db = client.db('sample_mflix');

    switch (method) {
        case 'GET':
            // Récupération d'un film par son ID
            try {
                const movie = await db.collection('movies').findOne({ _id: new ObjectId(idMovie) });
                if (!movie) {
                    // Si le film n'est pas trouvé, renvoie une erreur 404
                    return res.status(404).json({ message: 'Movie not found' });
                }
                // Si le film est trouvé, renvoie le film avec un statut 200
                return res.status(200).json(movie);
            } catch (error) {
                // Gestion des erreurs serveur
                return res.status(500).json({ message: error.message });
            }

        case 'POST':
            // Ajout d'un nouveau film
            try {
                // Parse le corps de la requête et insère le nouveau film
                const newMovie = await db.collection('movies').insertOne(JSON.parse(body));
                // Renvoie le film inséré avec un statut 201
                return res.status(201).json(newMovie.ops[0]);
            } catch (error) {
                // Gestion des erreurs de requête incorrecte
                return res.status(400).json({ message: error.message });
            }

        case "PUT":
            // Mise à jour d'un film existant
            try {
                const { title, genre, year } = JSON.parse(body); // Assurez-vous que le client envoie "application/json"
                await db.collection("movies").updateOne(
                    { _id: new ObjectId(idMovie) },
                    { $set: { title, genre, year: parseInt(year) } }
                );
                // Confirme la mise à jour avec succès
                res.status(200).json({ message: "Movie updated successfully" });
            } catch (error) {
                // Gestion des erreurs lors de la mise à jour
                res.status(500).json({ message: error.message });
            }
            break;

        case 'DELETE':
            // Suppression d'un film
            try {
                const deletedMovie = await db.collection('movies').deleteOne({ _id: new ObjectId(idMovie) });
                if (deletedMovie.deletedCount === 0) {
                    // Si aucun film correspondant n'est trouvé, renvoie une erreur 404
                    return res.status(404).json({ message: 'Movie not found' });
                }
                // Confirme la suppression avec succès
                return res.status(204).send();
            } catch (error) {
                // Gestion des erreurs serveur
                return res.status(500).json({ message: error.message });
            }

        default:
            // Méthode HTTP non prise en charge
            res.setHeader('Allow', ['GET', 'POST', 'PUT', 'DELETE']);
            return res.status(405).end(`Method ${method} Not Allowed`);
    }
}

