/**
* @swagger
* /api/movie/{idMovie}/comment/{idComment}:
*   get:
*     summary: Retrieve a specific comment by ID
*     description: Get detailed information about a specific comment by its ID.
*     parameters:
*       - in: path
*         name: idComment
*         required: true
*         schema:
*           type: string
*         description: The comment's ID
*     responses:
*       200:
*         description: Detailed information about the comment.
*         content:
*           application/json:
*             schema:
*               $ref: '#/components/schemas/Comment'
*       404:
*         description: Comment not found
*       500:
*         description: An error occurred on the server.
* 
*   put:
*     summary: Update a comment
*     description: Update details of an existing comment.
*     parameters:
*       - in: path
*         name: idComment
*         required: true
*         schema:
*           type: string
*         description: The comment's ID
*     requestBody:
*       required: true
*       content:
*         application/json:
*           schema:
*             $ref: '#/components/schemas/Comment'
*     responses:
*       200:
*         description: The comment was successfully updated.
*       404:
*         description: The comment was not found.
*       500:
*         description: An error occurred on the server.
* 
*   delete:
*     summary: Delete a comment
*     description: Remove a comment entry from the database.
*     parameters:
*       - in: path
*         name: idComment
*         required: true
*         schema:
*           type: string
*         description: The comment's ID
*     responses:
*       204:
*         description: The comment was successfully deleted.
*       404:
*         description: The comment was not found.
*       500:
*         description: An error occurred on the server.
*/

import { ObjectId } from 'mongodb';
import clientPromise from '../../../../../lib/mongodb';

// Handler pour les opérations GET, PUT, et DELETE sur un commentaire spécifique.
export default async function handler(req, res) {
    const { query: { idComment }, method, body } = req;

    // Initialisation du client MongoDB et sélection de la base de données.
    const client = await clientPromise;
    const db = client.db("sample_mflix");

    // Traitement en fonction du type de méthode HTTP reçue.
    switch (method) {
        case "GET":
            // Récupération d'un commentaire spécifique par son ID.
            try {
                const comment = await db.collection("comments").findOne({ _id: new ObjectId(idComment) });
                if (!comment) {
                    // Si aucun commentaire n'est trouvé, renvoie une erreur 404.
                    return res.status(404).json({ message: "Comment not found" });
                }
                // Si un commentaire est trouvé, le renvoie avec un statut 200.
                res.status(200).json(comment);
            } catch (error) {
                // Gestion des erreurs liées à la base de données ou autre.
                res.status(500).json({ message: error.message });
            }
            break;

        case "PUT":
            // Mise à jour d'un commentaire spécifique.
            try {
                // Ici le corps de la requête contient les données à mettre à jour.
                const updateData = req.body;
                const updatedComment = await db.collection("comments").updateOne(
                    { _id: new ObjectId(idComment) },
                    { $set: updateData }
                );
                
                if (!updatedComment.matchedCount) {
                    // Si aucun commentaire correspondant n'est trouvé, renvoie une erreur 404.
                    return res.status(404).json({ message: "Comment not found" });
                }
                // Si la mise à jour est réussie, renvoie une confirmation.
                res.status(200).json({ message: "Comment updated successfully" });
            } catch (error) {
                // Gestion des erreurs lors de la mise à jour.
                res.status(500).json({ message: error.message });
            }
            break;

        case "DELETE":
            // Suppression d'un commentaire spécifique.
            try {
                const deletedComment = await db.collection("comments").deleteOne({ _id: new ObjectId(idComment) });
                if (!deletedComment.deletedCount) {
                    // Si le commentaire à supprimer n'est pas trouvé, renvoie une erreur 404.
                    return res.status(404).json({ message: "Comment not found" });
                }
                // Si la suppression est réussie, renvoie un statut 204 (aucun contenu).
                res.status(204).end();
            } catch (error) {
                // Gestion des erreurs lors de la suppression.
                res.status(500).json({ message: error.message });
            }
            break;

        default:
            // Si la méthode HTTP n'est pas prise en charge, renvoie une erreur 405.
            res.setHeader('Allow', ['GET', 'POST', 'PUT', 'DELETE']);
            res.status(405).end(`Method ${method} Not Allowed`);
    }
}

