import { ObjectId } from 'mongodb';
import clientPromise from '../../../../../lib/mongodb';

// Ce handler gère la création (POST) de nouveaux commentaires.
export default async function handler(req, res) {
    // Bloque les méthodes autres que POST.
    if (req.method !== 'POST') {
        return res.status(405).end(`Method ${req.method} Not Allowed`);
    }

    const client = await clientPromise;
    const db = client.db('sample_mflix');

    try {
        // Extraction des champs nécessaires du corps de la requête.
        // Assure que le corps de la requête est déjà parsé en objet JSON par Next.js.
        const { movieId, name, email, text, date } = req.body;

        // Validation pour s'assurer que tous les champs nécessaires sont fournis.
        if (!movieId || !name || !email || !text || !date) {
            // Si des champs sont manquants, renvoie une erreur 400 avec un message.
            return res.status(400).json({ message: "Tous les champs sont requis" });
        }

        // Insérer le nouveau commentaire dans la base de données.
        const result = await db.collection('comments').insertOne({
            movie_id: new ObjectId(movieId), // Conversion de movieId en ObjectId.
            name,
            email,
            text,
            date
        });

        // Récupérer le commentaire inséré pour le renvoyer dans la réponse.
        const insertedComment = await db.collection('comments').findOne({ _id: result.insertedId });

        // Renvoie le commentaire inséré avec un statut 201 pour indiquer la création réussie.
        res.status(201).json(insertedComment);
    } catch (error) {
        // Gestion des erreurs liées à la base de données ou autre avec un statut 500.
        res.status(500).json({ message: error.message });
    }
}
