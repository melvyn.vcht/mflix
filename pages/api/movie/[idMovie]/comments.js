/**
* @swagger
* /api/movie/{idMovie}/comments:
*   get:
*     summary: Retrieve all comments for a specific movie
*     description: Fetches all comments associated with a specific movie by its ID.
*     parameters:
*       - in: path
*         name: idMovie
*         required: true
*         schema:
*           type: string
*         description: The unique identifier of the movie
*     responses:
*       200:
*         description: An array of comments related to the movie.
*         content:
*           application/json:
*             schema:
*               type: array
*               items:
*                 type: object
*                 properties:
*                   name:
*                     type: string
*                     description: Name of the commenter
*                   email:
*                     type: string
*                     description: Email of the commenter
*                   text:
*                     type: string
*                     description: The comment text
*                   date:
*                     type: string
*                     format: date-time
*                     description: Date when the comment was posted
*       404:
*         description: Movie not found
*       500:
*         description: Error fetching comments
*/

import { ObjectId } from 'mongodb';
import clientPromise from '../../../../lib/mongodb';

// Handler pour récupérer tous les commentaires d'un film spécifique par son ID.
export default async function handler(req, res) {
    const { query: { idMovie } } = req; // Extraction de l'ID du film depuis la requête.

    const client = await clientPromise;
    const db = client.db("sample_mflix");

    try {
        // Recherche dans la collection 'comments' tous les commentaires associés à l'ID du film.
        const comments = await db.collection("comments").find({ movie_id: new ObjectId(idMovie) }).toArray();

        // Si la recherche est réussie, renvoie les commentaires avec un statut 200.
        res.status(200).json(comments);
    } catch (error) {
        // Gestion des erreurs potentielles (e.g., problème de connexion à la base de données).
        res.status(500).json({ message: error.message });
    }
}

