// Importation du client MongoDB et configuration de la connexion à la base de données.
import clientPromise from '../../../lib/mongodb';

// Le gestionnaire asynchrone pour les requêtes HTTP vers '/api/movie'.
export default async function handler(req, res) {
    // Seules les requêtes POST sont autorisées pour ce endpoint, afin de créer de nouveaux films.
    if (req.method !== 'POST') {
        // Si une méthode autre que POST est utilisée, renvoie un code de statut 405.
        return res.status(405).end(`Method ${req.method} Not Allowed`);
    }

    // Établissement de la connexion à la base de données MongoDB.
    const client = await clientPromise;
    const db = client.db('sample_mflix');

    try {
        // Assurez-vous que le corps de la requête contient les informations nécessaires pour créer un film.
        // C'est une validation simple. Vous pourriez vouloir vérifier chaque champ spécifiquement.
        if (!req.body.title || !req.body.genre) {
            return res.status(400).json({ message: "Missing required movie information" });
        }

        // Insère le nouveau film dans la collection 'movies' en utilisant les données fournies dans le corps de la requête.
        const result = await db.collection('movies').insertOne(req.body);

        // Récupération du film inséré à partir de la base de données pour le renvoyer dans la réponse HTTP.
        const movie = await db.collection('movies').findOne({ _id: result.insertedId });

        // Renvoie le film inséré avec un code de statut 201, indiquant la création réussie.
        res.status(201).json(movie);
    } catch (error) {
        // Gestion des erreurs liées à la base de données ou aux opérations du serveur.
        res.status(500).json({ message: error.message });
    }
}
