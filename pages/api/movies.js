/**
* @swagger
* /api/movies:
*   get:
*     summary: Retrieve all movies
*     description: Get a list of all movies in the database.
*     responses:
*       200:
*         description: A list of movies.
*         content:
*           application/json:
*             schema:
*               type: array
*               items:
*                 $ref: '#/components/schemas/Movie'
*       500:
*         description: An error occurred on the server.
*/

// Importation du client MongoDB promisifié
import clientPromise from "../../lib/mongodb";

// Le gestionnaire asynchrone pour les requêtes à cet endpoint
export default async function handler(req, res) {
    // Établissement de la connexion au client MongoDB
    const client = await clientPromise;

    // Accès à la base de données 'sample_mflix'
    const db = client.db("sample_mflix");

    // Tentative de récupération des films. Limitez le nombre de résultats pour éviter de surcharger la réponse.
    // Vous pouvez ajuster le nombre selon vos besoins ou implémenter une pagination.
    try {
        const movies = await db.collection("movies").find({}).limit(20).toArray();
        res.status(200).json({ status: 'success', data: movies });
    } catch (error) {
        // Gestion des erreurs (par exemple, problème de connexion à la base de données)
        res.status(500).json({ status: 'error', message: 'Unable to fetch the movies' });
    }
}
