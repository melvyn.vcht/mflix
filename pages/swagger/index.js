// pages/swagger/index.jsx
import Head from 'next/head'; // Importation du composant Head pour personnaliser les métadonnées de la page.
import SwaggerUI from 'swagger-ui-react'; // Importation de Swagger UI pour afficher la documentation interactive de l'API.
import 'swagger-ui-react/swagger-ui.css'; // Importation du style CSS par défaut de Swagger UI.

// Composant fonctionnel Swagger pour afficher la page de documentation de l'API.
const Swagger = () => {
  return (
    <div>
      <Head>
        <title>API Documentation</title> 
        <meta name="description" content="Explore the MFLIX API with our interactive Swagger documentation." />
        <link rel="icon" href="/favicon.svg" /> 
      </Head>
      <SwaggerUI url="/api/doc" />
    </div>
  );
};

export default Swagger;
